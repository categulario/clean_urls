use std::env::args;
use std::vec;
use std::collections::HashMap;
use std::cmp::Reverse;

use csvsc::prelude::*;
use csvsc::error::Result as CsvscResult;
use csv::{ReaderBuilder, StringRecord};
use http::Uri;

mod transform;

use transform::extract_urls_from_line;

fn ensure_scheme(val: &str) -> CsvscResult<String> {
    if val.starts_with("http://") || val.starts_with("https://") {
        Ok(val.to_owned())
    } else {
        Ok(String::from("http://") + val)
    }
}

fn main() {
    let args: Vec<_> = args().collect();
    let input_file = args.get(1).expect("Needs a path as first argument");
    let mut hostnames = HashMap::new();

    let chain = InputStreamBuilder::from_readers(vec![
        ReaderBuilder::new()
            .delimiter(b'\t')
            .from_path(&input_file)
            .unwrap().into(),
    ]).build().unwrap()

        // split lines if multiple urls are in the same line
        .map_row(|headers, row| {
            let text = headers.get_field(&row, "url").unwrap();
            let urls = extract_urls_from_line(text);
            let new_rows: Vec<_> = urls.iter()
                .map(|u| Ok(StringRecord::from(vec![u])))
                .collect();

            Ok(new_rows.into_iter())
        }, |headers| headers.clone())

        // make sure urls start with http://
        .map_col("url", ensure_scheme)

        // Report uris that are not valid
        .map_col("url", |val| {
            match val.parse::<Uri>() {
                Ok(_) => Ok(val.to_owned()),
                Err(e) => {
                    dbg!(val);
                    dbg!(e);

                    panic!();
                }
            }
        })

        .flush(Target::path("01_clean_urls.txt")).unwrap()

        // remove the scheme and the path, keep the hostname
        .map_col("url", |url| {
            let url = url.trim_start_matches("http://")
                .trim_start_matches("https://")
                .trim_start_matches("www.");

            let hostname = url.split('/').next().unwrap();

            Ok(hostname.to_string())
        })

        .review(|headers, row| {
            if let Ok(row) = row {
                let hostname = headers.get_field(row, "url").unwrap().to_owned();

                let entry = hostnames.entry(hostname).or_insert(0);
                *entry += 1;
            }
        })

        .into_iter();

    for item in chain {
        if let Err(e) = item {
            eprintln!("{e}");
            break;
        }
    }

    let sorted_hostnames = {
        let mut list: Vec<_> = hostnames.into_iter().collect();

        list.sort_unstable_by_key(|x| Reverse(x.1));

        list
    };

    let num_sources = sorted_hostnames.len();

    println!("noticias,sitio");

    for (hostname, count) in sorted_hostnames {
        println!("{count},{hostname}");
    }

    eprintln!("finished!");
    eprintln!("Found {num_sources} different sources");
}
