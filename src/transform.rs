pub fn extract_urls_from_line(line: &str) -> Vec<String> {
    line.to_lowercase().split(" y ")

        // getting more urls
        .map(|x| x.split(" y")).flatten()
        .map(|x| x.split("´y ")).flatten()
        .map(|x| x.split(' ')).flatten()
        .map(|x| x.split("https://")).flatten()

        // some urls happen to start with a /
        .map(|x| x.trim_start_matches('/'))

        // clean whitespaces
        .map(|x| x.trim())

        // replace some chars, this outputs strings
        .map(|x| x.replace("á", "%C3%A1"))
        .map(|x| x.replace("é", "%C3%A9"))
        .map(|x| x.replace("í", "%C3%AD"))
        .map(|x| x.replace("ó", "%C3%B3"))
        .map(|x| x.replace("ú", "%C3%BA"))
        .map(|x| x.replace("ñ", "%C3%B1"))
        .map(|x| x.replace("📹", "%F0%9F%93%B9"))
        .map(|x| x.replace("–", "")) // dont really know what this is
        .map(|x| x.replace("http:///", "http://"))

        // cleaning of results
        .filter(|x| !x.is_empty())
        .collect()
}

#[cfg(test)]
mod tests {
    use super::*;

    const CASE_01: &str = "https://www.razon.com.mx/mexico/incendian-patrullas-por-atropellar-a-menor-en-san-martin-texmelucan/ y https://www.sinembargo.mx/01-01-2019/3517530";
    const CASE_02: &str = "http://www.puebla321.com/2019/03/22/taxista-muere-atropellado-tras-bajar-de-su-unidad-para-revisar-su-choque/´y http://elincorrecto.mx/2019/03/taxista-muere-atropellado-en-la-autopista-intento-huir-tras-choque-con-un-trailer/";

    #[test]
    fn urls_are_extracted() {
        assert_eq!(extract_urls_from_line(CASE_01), vec![
            "https://www.razon.com.mx/mexico/incendian-patrullas-por-atropellar-a-menor-en-san-martin-texmelucan/",
            "https://www.sinembargo.mx/01-01-2019/3517530",
        ]);

        assert_eq!(extract_urls_from_line(CASE_02), vec![
            "http://www.puebla321.com/2019/03/22/taxista-muere-atropellado-tras-bajar-de-su-unidad-para-revisar-su-choque/",
            "http://elincorrecto.mx/2019/03/taxista-muere-atropellado-en-la-autopista-intento-huir-tras-choque-con-un-trailer/",
        ]);
    }
}
