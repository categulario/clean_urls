# clean urls

Toma como entrada un archivo de texto plano con una (o quizá varias) urls por
línea y lo convierte en un archivo de urls válidas, una por línea.

## Cómo usar

Necesitas [rust](https://rustup.rs), luego puedes hacer en el directorio del
proyecto:

    cargo build --release
    cargo run --release -- file_with_urls.txt > sources.csv

La primera línea del archivo de entrada debe tener el texto `url`.
